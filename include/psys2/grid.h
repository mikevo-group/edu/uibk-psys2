#ifndef PSYS2_GRID_H
#define PSYS2_GRID_H

#include <array>
#include <memory>
#include <mpi.h>
#include <vector>

#include "algorithm.h"
#include "array_helper.h"

namespace psys2 {

/**
 * Type used to store the size of the dimensions of the grid. The size of
 * each dimension is stored in the respective index of the type.
 */
template <size_t dim>
using grid_size_t = std::array<size_t, dim>;

/**
 * Type used to specify and offset to an index of size. This renaming is done
 * for readability purposes.
 */
template <size_t dim>
using grid_offset_t = std::array<int, dim>;

/**
 * Type used to hold an index/position of the grid. This type has non-const
 * elements and can be modified after creation.
 */
template <size_t dim>
using grid_index_t = std::array<size_t, dim>;

/**
 * Function to get the linear size of a given N-dimensional size
 * specification.
 * @param  size N-dimensional size specification of a grid.
 * @return      The linear size needed to store the grid.
 */
template <size_t dim>
auto linear_size(const grid_size_t<dim>& size) {
  auto linear_size = 1u;

  for (const auto dimension_size : size) {
    linear_size *= dimension_size;
  }

  return linear_size;
}

/**
 * Checks if an index is a bound cell given the virtual bounds defined by start
 * index and end index.
 * @param  start Start index of the virtual bounds (included).
 * @param  end   End index of the virtual bounds (included).
 * @param  index Index to be checked.
 * @return       True if the index is a bounde cell, otherwise false.
 */
template <size_t dim>
bool is_bound_index(const grid_index_t<dim>& start,
                    const grid_index_t<dim>& end,
                    const grid_index_t<dim>& index) {
  for (size_t d = 0; d < dim; ++d) {
    auto start_index = start[d] == index[d];
    auto end_index = end[d] == index[d];

    if (start_index || end_index) {
      return true;
    }
  }
  return false;
}

/**
 * Iterates over all bound indcies and executes the function f with the current
 * index as an argument.
 * @param  start Start index of the virtual bounds (included).
 * @param  end   End index of the virtual bounds (included).
 * @param  f     The function to call.
 */
template <size_t dim>
struct for_each_bound_index {
  void operator()(const grid_index_t<dim>& start, const grid_index_t<dim>& end,
                  std::function<void(const grid_index_t<dim>&)> const& f);
};

template <>
struct for_each_bound_index<1> {
  void operator()(const grid_index_t<1>& start, const grid_index_t<1>& end,
                  std::function<void(const grid_index_t<1>&)> const& f) {
    f(start);
    f(end);
  };
};

template <>
struct for_each_bound_index<2> {
  void operator()(const grid_index_t<2>& start, const grid_index_t<2>& end,
                  std::function<void(const grid_index_t<2>&)> const& f) {
    psys2::grid_index_t<2> index;
    for (index[0] = start[0]; index[0] <= end[0]; ++index[0]) {
      for (index[1] = start[1]; index[1] <= end[1]; ++index[1]) {
        if (is_bound_index(start, end, index)) {
          f(index);
        } else {
          // skip inner cells
          index[1] = end[1] - 1;
        }
      }
    }
  };
};

template <>
struct for_each_bound_index<3> {
  void operator()(const grid_index_t<3>& start, const grid_index_t<3>& end,
                  std::function<void(const grid_index_t<3>&)> const& f) {
    psys2::grid_index_t<3> index;
    for (index[0] = start[0]; index[0] <= end[0]; ++index[0]) {
      for (index[1] = start[1]; index[1] <= end[1]; ++index[1]) {
        for (index[2] = start[2]; index[2] <= end[2]; ++index[2]) {
          if (is_bound_index(start, end, index)) {
            f(index);
          } else {
            // skip inner cells
            index[2] = end[2] - 1;
          }
        }
      }
    }
  }
};

/**
 * Calculates the size of virtual bounds for the given range.
 * @param  start Start index of the virtual bounds (included).
 * @param  end   End index of the virtual bounds (included).
 * @return       The number of virtual bound values inside the given range.
 */
template <size_t dim>
struct calculate_bound_size {
  int operator()(const grid_index_t<dim>& start, const grid_index_t<dim>& end);
};

template <>
struct calculate_bound_size<1> {
  int operator()(const grid_index_t<1>& start, const grid_index_t<1>& end) {
    auto dim0_length = end[0] - start[0] + 1;

    int max_size = dim0_length;
    int bound_size = 2;

    return std::min(max_size, bound_size);
  }
};

template <>
struct calculate_bound_size<2> {
  int operator()(const grid_index_t<2>& start, const grid_index_t<2>& end) {
    auto dim0_length = end[0] - start[0] + 1;
    auto dim1_length = end[1] - start[1] + 1;

    int max_size = dim0_length * dim1_length;
    int bound_size =
        2 * dim0_length + 2 * (dim1_length - 2);  // -2: subtract corner points

    return std::min(max_size, bound_size);
  }
};

template <>
struct calculate_bound_size<3> {
  int operator()(const grid_index_t<3>& start, const grid_index_t<3>& end) {
    auto dim0_length = end[0] - start[0] + 1;
    auto dim1_length = end[1] - start[1] + 1;
    auto dim2_length = end[2] - start[2] + 1;

    int max_size = dim0_length * dim1_length * dim2_length;
    int bound_size = 2 * dim0_length * dim1_length +
                     2 * dim0_length * (dim2_length - 2) +
                     2 * (dim1_length - 2) * (dim2_length - 2);

    return std::min(max_size, bound_size);
  }
};

/**
 * Creates a pair of grids with the specified size.
 * @param  size Size of each grid.
 * @return      The two grids.
 */
template <typename grid_t>
auto init_grid(const typename grid_t::grid_size_type& size) {
  auto grid0 = std::make_shared<grid_t>(size);
  auto grid1 = std::make_shared<grid_t>(size);

  return std::make_pair(grid0, grid1);
}

/**
 * The Grid class represents an N-dimensional data structure. It linearizes
 * the data and stores them.
 */
template <typename Cell, size_t dim>
class Grid {
 public:
  using cell_t = Cell;
  using grid_size_type = grid_size_t<dim>;
  using index_t = grid_index_t<dim>;

  template <size_t d>
  friend int mpi_send_data(Grid<double, d>& grid, int destination,
                           MPI_Comm comm);
  template <size_t d>
  friend int mpi_recv_data(Grid<double, d>& grid, int source, MPI_Comm comm);
  template <size_t d>
  friend int mpi_isend_data(Grid<double, d>& grid, int destination,
                            MPI_Comm comm, MPI_Request* request);
  template <size_t d>
  friend int mpi_irecv_data(Grid<double, d>& grid, int source, MPI_Comm comm,
                            MPI_Request* request);

  /// Number of dimensions the grid has.
  static constexpr auto DIMENSIONS = dim;

  /**
   * Constructs a new N-dimensional grid and initializes the cells.
   * @param  size N-dimensional size specification of a grid.
   * @return      The grid object.
   */
  explicit Grid(const grid_size_type& size)
      : data(linear_size(size)), dimension_size(size) {}

  /**
   * Data access method of the grid.
   *
   * @param  index The N-dimensional index specifying the position in the
   *               grid.
   * @return       The cell item at this position.
   */
  virtual Cell& at(const grid_index_t<dim>& index) {
    return data.at(linear_index_of(index));
  }

  /**
   * Data access method of the grid.
   *
   * @param  index The N-dimensional index specifying the position in the
   *               grid.
   * @return       The cell item at this position.
   */
  Cell& at(const std::valarray<size_t>& index) {
    if (index.size() != dim) {
      throw std::invalid_argument(
          "Array dimension does not match valarray size.");
    }

    return at(valarray_to_array<size_t, dim>(index));
  }

  /**
   * Stores the given item at the specified index.
   * @param index The N-dimensional index specifying the position in the
   *              grid.
   * @param item  The cell item to be stored.
   * @return      True if the write was successful.
   */
  virtual bool store(const grid_index_t<dim>& index, Cell item) {
    data[linear_index_of(index)] = item;
    return true;
  }

  /**
   * Stores the given item at the specified index.
   * @param index The N-dimensional index specifying the position in the
   *              grid.
   * @param item  The cell item to be stored.
   * @return      True if the write was successful.
   */
  bool store(const std::valarray<size_t>& index, Cell item) {
    if (index.size() != dim) {
      throw std::invalid_argument(
          "Array dimension does not match valarray size.");
    }

    return store(valarray_to_array<size_t, dim>(index), item);
  }

  /**
   * Linear index calculation method of the grid.
   *
   * @param  index The N-dimensional index specifying the position in the
   *               grid.
   * @return       The linear position of that index.
   */
  const auto linear_index_of(const grid_index_t<dim>& index) const {
    // This could be a performance issue
    auto pos = index[DIMENSIONS - 1];
    auto size = dimension_size[DIMENSIONS - 1];

    for (long i = DIMENSIONS - 2; i >= 0; --i) {
      pos += index[i] * size;
      size *= dimension_size[i];
    }

    return pos;
  }

  /**
   * Gets a subgrid of the grid.
   * @param  start Start index (including)
   * @param  end   End index (including)
   * @return       A shared pointer to the allocated subgrid.
   */
  std::shared_ptr<Grid> subgrid(const grid_index_t<dim>& start,
                                const grid_index_t<dim>& end) {
    using val_index_t = std::valarray<size_t>;
    auto start_index = array_to_valarray<size_t, dim>(start);
    auto end_index = array_to_valarray<size_t, dim>(end);

    val_index_t size = end_index - start_index;
    size += 1;

    auto grid_size = valarray_to_array<size_t, dim>(size);

    auto subgrid = std::make_shared<Grid>(grid_size);

    struct for_each_index<dim> for_each;
    for_each(
        start, end,
        [this, subgrid, &start_index](const grid_index_t<dim>& index) -> void {
          auto dest_index = array_to_valarray<size_t, dim>(index);
          dest_index -= start_index;
          subgrid->store(dest_index, at(index));
        });

    return subgrid;
  }

  /**
   * Replaces the part of this grid, starting with the given start index, with
   * the whole given subgrid.
   * @param subgrid The subgrid used for replacement.
   * @param start   The start index position of this grid where the given
   *                subgrid should replace this grid.
   */
  void replace_with_subgrid(std::shared_ptr<Grid> subgrid,
                            const grid_index_t<dim>& start) {
    using val_index_t = std::valarray<size_t>;
    const auto grid_size = subgrid->dimension_size;
    auto size = array_to_valarray<size_t, dim>(grid_size);

    auto start_index = array_to_valarray<size_t, dim>(start);
    val_index_t end_index = start_index + size;
    end_index -= 1;
    auto end = valarray_to_array<size_t, dim>(end_index);

    struct for_each_index<dim> for_each;
    for_each(start, end,
             [this, subgrid, &start_index](const grid_index_t<dim>& index) {
               auto src_index = array_to_valarray<size_t, dim>(index);
               src_index -= start_index;
               store(index, subgrid->at(src_index));
             });
  }

  /**
   * Returns the entries in the grid contained in the virtual boundary spanned
   * by the start index and end index.
   * @param start Start index of boundary to read (included in boundary)
   * @param end   End index of boundary to read (included in boundary)
   */
  std::shared_ptr<std::vector<cell_t>> boundary_data(const index_t& start,
                                                     const index_t& end) {
    struct for_each_bound_index<dim> for_each_bound;
    auto bound_cell_vector = std::make_shared<std::vector<cell_t>>();

    for_each_bound(start, end, [this, bound_cell_vector](const index_t& index) {
      auto cell = at(index);
      bound_cell_vector->push_back(cell);
    });

    return bound_cell_vector;
  }

  /**
   * Replace the entries of the grid contained in the virtual boundary spanned
   * by the start index and end index.
   * @param  data  The data vector containing the data to be written.
   * @param  start Start index of boundary to write (included in boundary)
   * @param end    End index of boundary to read (included in boundary)
   * @return       True if writing was successful for all cells, otherwise
   * false.
   */
  bool replace_with_boundary_data(std::shared_ptr<std::vector<cell_t>> data,
                                  const grid_index_t<dim>& start,
                                  const grid_index_t<dim>& end) {
    struct for_each_bound_index<dim> for_each_bound;
    int all_successful = true;

    size_t i = 0;
    for_each_bound(start, end,
                   [this, data, &all_successful, &i](const index_t& index) {
                     auto successful = store(index, data->at(i));
                     if (!successful) {
                       all_successful = false;
                     }
                     ++i;
                   });

    return all_successful;
  }

  /**
   * Accesses the size of the grid.
   * @return The size of the grid.
   */
  virtual const grid_size_type size() const { return dimension_size; }

 private:
  std::vector<Cell> data;

  /// The size of each dimension.
  const grid_size_type dimension_size;
};
}  // namespace psys2

#endif  // PSYS2_GRID_H