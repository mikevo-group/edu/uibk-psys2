#include <gtest/gtest.h>

#include "psys2/grid_helper.h"

namespace psys2 {

TEST(GridHelper, sub_valarray) {
  constexpr size_t DIM = 7;

  struct sub_valarray<DIM> sub_valarray;

  grid_size_t<DIM> array;
  for (int i = 0; i < DIM; ++i) {
    array[i] = i + 1;
  }

  int length = 5;
  auto sub_array = sub_valarray(array, length);

  EXPECT_EQ(length, sub_array.size());

  for (int i = 0; i < length; ++i) {
    EXPECT_EQ(array[i], sub_array[i]);
  }
}

TEST(GridHelper, calculate_grid_size) {
  constexpr size_t DIM = 7;

  struct calculate_grid_size<DIM> calculate_grid_size;

  grid_index_t<DIM> start = {{1, 2, 3, 4, 5, 6, 7}};
  grid_index_t<DIM> end = {{8, 10, 12, 14, 16, 18, 20}};
  grid_index_t<DIM> expected = {{8, 9, 10, 11, 12, 13, 14}};

  auto actual = calculate_grid_size(start, end);

  for (int i = 0; i < expected.size(); ++i) {
    EXPECT_EQ(expected[i], actual[i]);
  }
}

TEST(GridHelper, get_tile_size_1D) {
  constexpr size_t DIM = 3;
  constexpr size_t SUBDIV_DIM = 1;

  struct get_tile_size<DIM, SUBDIV_DIM> get_tile_size;

  grid_size_t<DIM> grid_size = {
      {31, 37, 41}};  // prime to ensure bad split condition
  int num_of_tiles = 9;
  grid_size_t<SUBDIV_DIM> expected = {{3}};
  auto actual = get_tile_size(grid_size, num_of_tiles);

  EXPECT_EQ(expected.size(), actual.size());
  for (int i = 0; i < expected.size(); ++i) {
    EXPECT_EQ(expected[i], actual[i]);
  }
}

TEST(GridHelper, get_tile_size_2D) {
  constexpr size_t DIM = 3;
  constexpr size_t SUBDIV_DIM = 2;

  struct get_tile_size<DIM, SUBDIV_DIM> get_tile_size;

  grid_size_t<DIM> grid_size = {
      {31, 37, 41}};  // prime to ensure bad split condition
  int num_of_tiles = 9;
  grid_size_t<SUBDIV_DIM> expected = {{10, 12}};
  auto actual = get_tile_size(grid_size, num_of_tiles);

  EXPECT_EQ(expected.size(), actual.size());
  for (int i = 0; i < expected.size(); ++i) {
    EXPECT_EQ(expected[i], actual[i]);
  }
}

TEST(GridHelper, get_read_indices_1D) {
  constexpr size_t DIM = 3;
  constexpr size_t SUBDIV_DIM = 1;

  using index_t = grid_index_t<DIM>;
  struct get_read_indices<DIM, SUBDIV_DIM> get_read_indices;

  grid_size_t<DIM> grid_size = {
      {31, 37, 41}};  // prime to ensure bad split condition
  int num_of_tiles = 9;

  indices_map_t<DIM> expected;
  expected[0] = std::make_pair<index_t, index_t>({{0, 0, 0}}, {{4, 36, 40}});
  expected[1] = std::make_pair<index_t, index_t>({{3, 0, 0}}, {{7, 36, 40}});
  expected[2] = std::make_pair<index_t, index_t>({{6, 0, 0}}, {{10, 36, 40}});
  expected[3] = std::make_pair<index_t, index_t>({{9, 0, 0}}, {{13, 36, 40}});
  expected[4] = std::make_pair<index_t, index_t>({{12, 0, 0}}, {{16, 36, 40}});
  expected[5] = std::make_pair<index_t, index_t>({{15, 0, 0}}, {{19, 36, 40}});
  expected[6] = std::make_pair<index_t, index_t>({{18, 0, 0}}, {{22, 36, 40}});
  expected[7] = std::make_pair<index_t, index_t>({{21, 0, 0}}, {{25, 36, 40}});
  expected[8] = std::make_pair<index_t, index_t>({{24, 0, 0}}, {{30, 36, 40}});

  auto actual = get_read_indices(grid_size, num_of_tiles);

  EXPECT_EQ(expected, actual);
}

TEST(GridHelper, get_read_indices_2D) {
  constexpr size_t DIM = 3;
  constexpr size_t SUBDIV_DIM = 2;

  using index_t = grid_index_t<DIM>;
  struct get_read_indices<DIM, SUBDIV_DIM> get_read_indices;

  grid_size_t<DIM> grid_size = {
      {31, 37, 41}};  // prime to ensure bad split condition
  int num_of_tiles = 9;

  indices_map_t<DIM> expected;
  expected[0] = std::make_pair<index_t, index_t>({{0, 0, 0}}, {{11, 13, 40}});
  expected[1] = std::make_pair<index_t, index_t>({{10, 0, 0}}, {{21, 13, 40}});
  expected[2] = std::make_pair<index_t, index_t>({{20, 0, 0}}, {{30, 13, 40}});
  expected[3] = std::make_pair<index_t, index_t>({{0, 12, 0}}, {{11, 25, 40}});
  expected[4] = std::make_pair<index_t, index_t>({{10, 12, 0}}, {{21, 25, 40}});
  expected[5] = std::make_pair<index_t, index_t>({{20, 12, 0}}, {{30, 25, 40}});
  expected[6] = std::make_pair<index_t, index_t>({{0, 24, 0}}, {{11, 36, 40}});
  expected[7] = std::make_pair<index_t, index_t>({{10, 24, 0}}, {{21, 36, 40}});
  expected[8] = std::make_pair<index_t, index_t>({{20, 24, 0}}, {{30, 36, 40}});

  auto actual = get_read_indices(grid_size, num_of_tiles);

  EXPECT_EQ(expected, actual);
}

TEST(GridHelper, get_write_indices_1D) {
  constexpr size_t DIM = 3;
  constexpr size_t SUBDIV_DIM = 1;

  using index_t = grid_index_t<DIM>;
  struct get_write_indices<DIM, SUBDIV_DIM> get_write_indices;

  grid_size_t<DIM> grid_size = {
      {31, 37, 41}};  // prime to ensure bad split condition
  int num_of_tiles = 9;

  indices_map_t<DIM> expected;
  expected[0] = std::make_pair<index_t, index_t>({{1, 1, 1}}, {{3, 35, 39}});
  expected[1] = std::make_pair<index_t, index_t>({{4, 1, 1}}, {{6, 35, 39}});
  expected[2] = std::make_pair<index_t, index_t>({{7, 1, 1}}, {{9, 35, 39}});
  expected[3] = std::make_pair<index_t, index_t>({{10, 1, 1}}, {{12, 35, 39}});
  expected[4] = std::make_pair<index_t, index_t>({{13, 1, 1}}, {{15, 35, 39}});
  expected[5] = std::make_pair<index_t, index_t>({{16, 1, 1}}, {{18, 35, 39}});
  expected[6] = std::make_pair<index_t, index_t>({{19, 1, 1}}, {{21, 35, 39}});
  expected[7] = std::make_pair<index_t, index_t>({{22, 1, 1}}, {{24, 35, 39}});
  expected[8] = std::make_pair<index_t, index_t>({{25, 1, 1}}, {{29, 35, 39}});

  auto actual = get_write_indices(grid_size, num_of_tiles);

  EXPECT_EQ(expected, actual);
}

TEST(GridHelper, get_write_indices_2D) {
  constexpr size_t DIM = 3;
  constexpr size_t SUBDIV_DIM = 2;

  using index_t = grid_index_t<DIM>;
  struct get_write_indices<DIM, SUBDIV_DIM> get_write_indices;

  grid_size_t<DIM> grid_size = {
      {31, 37, 41}};  // prime to ensure bad split condition
  int num_of_tiles = 9;

  indices_map_t<DIM> expected;
  expected[0] = std::make_pair<index_t, index_t>({{1, 1, 1}}, {{10, 12, 39}});
  expected[1] = std::make_pair<index_t, index_t>({{11, 1, 1}}, {{20, 12, 39}});
  expected[2] = std::make_pair<index_t, index_t>({{21, 1, 1}}, {{29, 12, 39}});
  expected[3] = std::make_pair<index_t, index_t>({{1, 13, 1}}, {{10, 24, 39}});
  expected[4] = std::make_pair<index_t, index_t>({{11, 13, 1}}, {{20, 24, 39}});
  expected[5] = std::make_pair<index_t, index_t>({{21, 13, 1}}, {{29, 24, 39}});
  expected[6] = std::make_pair<index_t, index_t>({{1, 25, 1}}, {{10, 35, 39}});
  expected[7] = std::make_pair<index_t, index_t>({{11, 25, 1}}, {{20, 35, 39}});
  expected[8] = std::make_pair<index_t, index_t>({{21, 25, 1}}, {{29, 35, 39}});

  auto actual = get_write_indices(grid_size, num_of_tiles);

  EXPECT_EQ(expected, actual);
}

}  // namespace psys2