#!/bin/sh -e

build/psys2 $@
for i in {1,2,4,8} ; do
	mpirun -np $i build/psys2_mpi $@
done