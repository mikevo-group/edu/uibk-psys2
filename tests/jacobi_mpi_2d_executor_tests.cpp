#include <gtest/gtest.h>

#include "psys2/jacobi_mpi_executor.h"
#include "psys2/jacobi_serial_executor.h"

namespace psys2 {

const MPI_Comm communicator = MPI_COMM_WORLD;

TEST(JacobiMPI2DExecutor, execute_2D__comp_to_serial) {
  MPI_Barrier(communicator);
  constexpr size_t DIM = 2;
  constexpr double ERROR = 123.;
  using grid_t = BoundedGrid<double, DIM>;
  using grid_ptr_t = JacobiMPIExecutor<DIM, 2>::grid_ptr_t;
  using range_pair = std::pair<grid_t::index_t, grid_t::index_t>;

  grid_t::grid_size_type size = {{109, 113}};  // primes for bad split condition
  grid_t::bound_t bounds = {{1.0, 2.0, 3.0, 4.0}};

  grid_ptr_t grid = std::make_shared<grid_t>(size, bounds);
  JacobiMPIExecutor<DIM, 2> mpi_executor(grid, ERROR,
                                         mpi_comm_size(communicator));
  grid = mpi_executor.execute();

  if (mpi_rank(communicator) == ROOT_RANK) {
    auto grids = init_bounded_grid<grid_t>(size, bounds);
    JacobiSerialExecutor<grid_t> serial_executor(grids, ERROR);
    auto expected = serial_executor.execute();

    struct get_index_range<DIM> get_index_range;
    range_pair range = get_index_range(size);

    for_each_index2D(range.first, range.second,
                     [&](const grid_t::index_t &index) {
                       EXPECT_FLOAT_EQ(expected->at(index), grid->at(index));
                     });
  }
  MPI_Barrier(communicator);
}

TEST(JacobiMPI2DExecutor, execute_3D__comp_to_serial) {
  MPI_Barrier(communicator);
  constexpr size_t DIM = 3;
  constexpr double ERROR = 12345.;
  using grid_t = BoundedGrid<double, DIM>;
  using grid_ptr_t = JacobiMPIExecutor<DIM, 2>::grid_ptr_t;
  using range_pair = std::pair<grid_t::index_t, grid_t::index_t>;

  grid_t::grid_size_type size = {{31, 37, 41}};  // primes for bad split
  grid_t::bound_t bounds = {{1.0, 2.0, 3.0, 4.0, 5.0, 6.0}};

  grid_ptr_t grid = std::make_shared<grid_t>(size, bounds);
  JacobiMPIExecutor<DIM, 2> mpi_executor(grid, ERROR,
                                         mpi_comm_size(communicator));
  grid = mpi_executor.execute();

  if (mpi_rank(communicator) == ROOT_RANK) {
    auto grids = init_bounded_grid<grid_t>(size, bounds);
    JacobiSerialExecutor<grid_t> serial_executor(grids, ERROR);
    auto expected = serial_executor.execute();

    struct get_index_range<DIM> get_index_range;
    range_pair range = get_index_range(size);

    for_each_index3D(range.first, range.second,
                     [&](const grid_t::index_t &index) {
                       EXPECT_FLOAT_EQ(expected->at(index), grid->at(index));
                     });
  }
  MPI_Barrier(communicator);
}

}  // namespace psys2