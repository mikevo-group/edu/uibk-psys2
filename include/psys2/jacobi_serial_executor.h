#ifndef PSYS2_JACOBI_SERIAL_EXECUTOR_H
#define PSYS2_JACOBI_SERIAL_EXECUTOR_H

#include <cmath>
#include <memory>
#include <stdexcept>
#include <utility>

#include "algorithm.h"
#include "array_helper.h"
#include "bounded_grid.h"
#include "grid.h"
#include "jacobi.h"

namespace psys2 {

/**
 * JacobiSerialExecutor class used to execute the jacobi calculations for a
 * whole 3D grid with cell values of a2rithmetic type.
 */
template <typename grid_t,
          typename = typename std::enable_if<
              std::is_arithmetic<typename grid_t::cell_t>::value,
              typename grid_t::cell_t>::type>
class JacobiSerialExecutor {
 public:
  // Cell type of the grid
  using cell_t = typename grid_t::cell_t;

  /// The jacobi type used by the executor.
  using jacobi_t = Jacobi<cell_t, grid_t::DIMENSIONS>;

  /// The grid pointer type of the jacobi class.
  using grid_ptr_t = typename std::shared_ptr<grid_t>;

  // A pair of grids
  using grid_pair_t = typename std::pair<grid_ptr_t, grid_ptr_t>;

  /// The index type of the grid.
  using index_t = typename grid_t::index_t;

  /// Dimensions of the underlaying grid data structur
  static constexpr size_t DIMENSIONS = grid_t::DIMENSIONS;

  /**
   * Creates a new JacobiSerialExecutor using a standard grid as underlaying
   * grid.
   * @param  grid      A pair of grids for reading and writing.
   * @param  epsilon   The error threshold limiting the number of iterations.
   * @return           The new JacobiSerialExecutor object.
   */
  explicit JacobiSerialExecutor(grid_pair_t& grids, const cell_t& epsilon)
      : grids(grids), epsilon(epsilon), jacobi(grids.first) {
    auto grid0_size = grids.first->size();
    auto grid1_size = grids.second->size();

    if (grid0_size != grid1_size) {
      throw std::invalid_argument("Grids not of same size.");
    }

    constexpr bool lower_bound_ok = DIMENSIONS > 0;
    constexpr bool upper_bound_ok = DIMENSIONS < 4;
    constexpr bool size_ok = lower_bound_ok && upper_bound_ok;

    static_assert(size_ok, "Wrong dimension size");
  }

  /**
   * Starts the execution of the jacobi calculations. The calculations are
   * done for each cell in the grid and are repeated until the error falls
   * below the given epsilon value.
   * @return The grid containing the resulting values.
   */
  auto execute() {
    grid_size_t<DIMENSIONS> grid_size = grids.first->size();
    cell_t error;
    do {
      using range_pair = std::pair<index_t, index_t>;
      struct get_index_range<DIMENSIONS> get_index_range;

      range_pair range = get_index_range(grid_size);
      error = cell_t();

      struct for_each_index<DIMENSIONS> for_each;
      for_each(range.first, range.second,
               [this, &error](const index_t& index) -> void {
                 cell_t old_value = grids.first->at(index);
                 cell_t new_value = jacobi.calculate_value(index);
                 error += std::abs(old_value - new_value);

                 grids.second->store(index, new_value);
               });

      grids.first.swap(grids.second);
      jacobi.replace_grid(grids.first);
    } while (epsilon <= error);

    return grids.first;
  }  // namespace psys2

 private:
  /// read and write gird
  grid_pair_t grids;

  /// The error threshold value limiting the number of itrations.
  const cell_t& epsilon;

  /// The jacobi object used for the calculations.
  jacobi_t jacobi;
};
}  // namespace psys2

#endif  // PSYS2_JACOBI_SERIAL_EXECUTOR_H