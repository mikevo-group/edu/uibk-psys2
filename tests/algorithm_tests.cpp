#include <gtest/gtest.h>
#include <vector>

#include "psys2/algorithm.h"

namespace psys2 {
TEST(Algorithm, for_each_index1D) {
  using index_t = psys2::index1D_t;

  index_t start = {{1}};
  index_t end = {{5}};

  std::vector<index_t> expected;

  index_t index = start;
  for (size_t i = start[0]; i <= end[0]; ++i) {
    expected.push_back(index);
    index[0] += 1;
  }

  std::vector<index_t> indices;
  for_each_index1D(start, end, [&indices](const index_t& i) -> void {
    indices.push_back(i);
  });
  EXPECT_EQ(expected, indices);

  std::vector<index_t> indices_meta;
  for_each_index<1> for_each;
  for_each(start, end, [&indices_meta](const index_t& i) -> void {
    indices_meta.push_back(i);
  });
  EXPECT_EQ(expected, indices_meta);
}

TEST(Algorithm, for_each_index2D) {
  using index_t = psys2::index2D_t;

  index_t start = {{1, 2}};
  index_t end = {{5, 4}};

  std::vector<index_t> expected;

  index_t index = start;
  for (size_t i = start[0]; i <= end[0]; ++i) {
    index[1] = start[1];
    for (size_t j = start[1]; j <= end[1]; ++j) {
      expected.push_back(index);
      index[1] += 1;
    }
    index[0] += 1;
  }

  std::vector<index_t> indices;
  for_each_index2D(start, end, [&indices](const index_t& i) -> void {
    indices.push_back(i);
  });
  EXPECT_EQ(expected, indices);

  std::vector<index_t> indices_meta;
  for_each_index<2> for_each;
  for_each(start, end, [&indices_meta](const index_t& i) -> void {
    indices_meta.push_back(i);
  });
  EXPECT_EQ(expected, indices_meta);
}

TEST(Algorithm, for_each_index3D) {
  using index_t = psys2::index3D_t;

  index_t start = {{1, 2, 3}};
  index_t end = {{5, 4, 6}};

  std::vector<index_t> expected;

  index_t index = start;
  for (size_t i = start[0]; i <= end[0]; ++i) {
    index[1] = start[1];
    for (size_t j = start[1]; j <= end[1]; ++j) {
      index[2] = start[2];
      for (size_t k = start[2]; k <= end[2]; ++k) {
        expected.push_back(index);
        index[2] += 1;
      }
      index[1] += 1;
    }
    index[0] += 1;
  }

  std::vector<index_t> indices;
  for_each_index3D(start, end, [&indices](const index_t& i) -> void {
    indices.push_back(i);
  });
  EXPECT_EQ(expected, indices);

  std::vector<index_t> indices_meta;
  for_each_index<3> for_each;
  for_each(start, end, [&indices_meta](const index_t& i) -> void {
    indices_meta.push_back(i);
  });
  EXPECT_EQ(expected, indices_meta);
}
}  // namespace psys2