#ifndef PSYS2_JACOBI_MPI_NON_BLOCKING_EXECUTOR_H
#define PSYS2_JACOBI_MPI_NON_BLOCKING_EXECUTOR_H

#include <cmath>
#include <memory>
#include <mpi.h>
#include <set>
#include <stdexcept>
#include <utility>

#include "algorithm.h"
#include "array_helper.h"
#include "bounded_grid.h"
#include "grid.h"
#include "grid_helper.h"
#include "jacobi.h"
#include "mpi_helper.h"

namespace psys2 {
/**
 * JacobiMPINonBlockingExecutor class used to execute the jacobi calculations
 * with 1D or 2D subdivision for a whole grid with cell values of double type
 * using MPI for parallelization.
 */
template <size_t dim, size_t subdiv_dim>
class JacobiMPINonBlockingExecutor {
 public:
  /// The jacobi type used by the executor.
  using jacobi_t = Jacobi<double, dim>;

  /// The grid type
  using grid_t = Grid<double, dim>;

  /// The pointer type of the grid type
  using grid_ptr_t = std::shared_ptr<grid_t>;

  // A pair of grids
  using grid_pair_t = typename std::pair<grid_ptr_t, grid_ptr_t>;

  /// The index type of the grid.
  using index_t = typename grid_t::index_t;

  /// Dimensions of the underlaying grid data structur
  static constexpr size_t DIMENSIONS = dim;

  /// Dimensions of the subdivision
  static constexpr size_t SUBDIV_DIM = subdiv_dim;

  using range_pair = std::pair<index_t, index_t>;

  using sizes_map_t = std::map<int, index_t>;

  /**
   * Creates a new JacobiMPINonBlockingExecutor using the given grid as
   * underlaying grid.
   * @param  grid      The grid to apply jacobi on.
   * @param  epsilon   The error threshold limiting the number of iterations.
   * @return           The new JacobiMPINonBlockingExecutor object.
   */
  explicit JacobiMPINonBlockingExecutor(std::shared_ptr<grid_t> grid,
                                        const double& epsilon, int num_of_tiles)
      : grid_size(grid->size()),
        num_of_tiles(num_of_tiles),
        communicator(MPI_COMM_WORLD),
        executing(true),
        first_run(true),
        epsilon(epsilon) {
    constexpr bool lower_subdiv_bound_ok = SUBDIV_DIM > 0;
    constexpr bool upper_subdiv_bound_ok = SUBDIV_DIM < 3;
    constexpr bool subdiv_ok = lower_subdiv_bound_ok && upper_subdiv_bound_ok;

    static_assert(subdiv_ok, "Wrong subdivision size");

    constexpr bool lower_bound_ok = DIMENSIONS >= SUBDIV_DIM;
    constexpr bool upper_bound_ok = DIMENSIONS < 4;
    constexpr bool size_ok = lower_bound_ok && upper_bound_ok;

    static_assert(size_ok, "Wrong dimension size");

    world_rank = mpi_rank(communicator);
    world_size = mpi_comm_size(communicator);

    // store grid only in master
    if (world_rank == ROOT_RANK) {
      this->grid = grid;
    } else {
      this->grid = nullptr;
    }

    bool world_to_small = (num_of_tiles > world_size);
    if (world_to_small) {
      throw std::invalid_argument(
          "This implementation does not support more tiles than compute "
          "units");
    }

    auto max_num_of_tiles = sub_valarray(grid_size, SUBDIV_DIM).max() - 1;
    if (this->num_of_tiles > max_num_of_tiles) {
      this->num_of_tiles = max_num_of_tiles;
    }

    mpi_adjust_comm_size();

    // additional set up for root
    if (world_rank == ROOT_RANK) {
      // read and write indices
      read_indices = get_read_indices(grid_size, this->num_of_tiles);
      write_indices = get_write_indices(grid_size, this->num_of_tiles);

      for (auto index : write_indices) {
        auto rank = index.first;
        auto range = index.second;
        write_sizes[rank] = calculate_grid_size(range.first, range.second);
      }

      // MPI requests
      recv_data = std::vector<MPI_Request>(num_of_tiles);
    }
  }

  /**
   * Starts the execution of the jacobi calculations. The calculations are
   * done for each cell in the grid and are repeated until the error falls
   * below the given epsilon value.
   * @return The grid containing the resulting values.
   */
  grid_ptr_t execute() {
    if (!executing) {
      return nullptr;
    }

    grid_pair_t local_tiles;

    double error;
    double local_error;
    while (true) {
      if (break_condition_holds(error)) {
        break;
      }

      // distribute data
      if (world_rank == ROOT_RANK) {
        split_grid_and_distribute(local_tiles);
      } else {
        local_tiles.first = mpi_recv_grid<DIMENSIONS>(ROOT_RANK, communicator);
      }

      if (break_condition_holds(error)) {
        break;
      }

      // calculate jacobi
      double tmp_error = calculate_tile(local_tiles);

      // wait for allreduce to be done before overwriting error
      if (!first_run) {
        mpi_wait(&allreduce_delta);
      }

      local_error = tmp_error;

      if (break_condition_holds(error)) {
        break;
      }

      // collect data
      if (world_rank == ROOT_RANK) {
        collect_tiles_and_store(local_tiles.second);
      } else {
        mpi_isend_data(*local_tiles.second, ROOT_RANK, communicator,
                       &request_ignore);
      }

      mpi_iallreduce_delta(&local_error, communicator, &allreduce_delta,
                           &error);

      first_run = false;
    }

    return grid;
  }

 private:
  bool break_condition_holds(const double& error) {
    if (!first_run) {
      bool error_available = mpi_test(&allreduce_delta);
      bool threshold_reached = error_threshold_reached(error);
      return error_available && threshold_reached;
    }

    return false;
  }

  bool error_threshold_reached(const double& error) { return epsilon > error; }

  void mpi_adjust_comm_size() {
    if (world_size > num_of_tiles) {
      if (world_rank >= num_of_tiles) {
        MPI_Comm_split(MPI_COMM_WORLD, 1, world_rank, &communicator);
        executing = false;
      } else {
        MPI_Comm_split(MPI_COMM_WORLD, 0, world_rank, &communicator);
      }
    }

    world_rank = mpi_rank(communicator);
    world_size = mpi_comm_size(communicator);
  }

  void split_grid_and_distribute(grid_pair_t& grids) {
    for (auto index : read_indices) {
      auto dest = index.first;
      auto range = index.second;

      send_tiles[dest] = grid->subgrid(range.first, range.second);
      if (dest == ROOT_RANK) {
        grids.first = send_tiles[dest];
      } else {
        mpi_isend_grid(*send_tiles[dest], dest, communicator, &request_ignore,
                       &request_ignore);
      }
    }
  }

  void collect_tiles_and_store(grid_ptr_t tile) {
    // start recv data
    for (auto index : write_indices) {
      auto source = index.first;

      if (source == ROOT_RANK) {
        recv_tiles[source] = tile;
      } else {
        auto size = write_sizes[source];
        recv_tiles[source] = std::make_shared<grid_t>(size);
        mpi_irecv_data<DIMENSIONS>(*recv_tiles[source], source, communicator,
                                   &recv_data[source]);
      }
    }

    // check if all data received and store
    std::set<int> completed;
    while (completed.size() < num_of_tiles) {
      for (auto index : write_indices) {
        auto source = index.first;
        auto range = index.second;

        // if already completed continue
        bool already_completed = completed.find(source) != completed.end();
        if (!already_completed) {
          // test if recv completed, otherwise continue
          if (source != ROOT_RANK) {
            auto recv_completed = mpi_test(&recv_data[source]);
            if (!recv_completed) {
              continue;
            }
          }

          // replace tile in grid
          grid->replace_with_subgrid(recv_tiles[source], range.first);

          completed.insert(source);
        }
      }
    }
  }

  double calculate_tile(grid_pair_t& local_tiles) {
    double local_error = 0.0;

    auto tile_size = local_tiles.first->size();
    range_pair range = get_index_range(tile_size);

    auto output_size = tile_size;
    for (int i = 0; i < DIMENSIONS; ++i) {
      output_size[i] -= 2;
    }

    local_tiles.second =
        std::make_shared<Grid<double, DIMENSIONS>>(output_size);

    jacobi_t jacobi(local_tiles.first);
    for_each(
        range.first, range.second,
        [&jacobi, &local_tiles, &local_error](const index_t& index) -> void {
          double old_value = local_tiles.first->at(index);
          double new_value = jacobi.calculate_value(index);
          local_error += std::abs(old_value - new_value);

          auto store_index = array_to_valarray(index);
          store_index -= 1;
          local_tiles.second->store(store_index, new_value);
        });

    return local_error;
  }

  /// backend
  grid_ptr_t grid;
  grid_size_t<dim> grid_size;

  size_t world_rank;
  size_t world_size;
  size_t num_of_tiles;
  MPI_Comm communicator;

  bool executing;
  bool first_run;

  indices_map_t<dim> read_indices;
  indices_map_t<dim> write_indices;
  sizes_map_t write_sizes;

  MPI_Request allreduce_delta;
  MPI_Request request_ignore;
  std::vector<MPI_Request> recv_data;

  std::map<int, grid_ptr_t> send_tiles;
  std::map<int, grid_ptr_t> recv_tiles;

  struct for_each_index<DIMENSIONS> for_each;
  struct get_index_range<DIMENSIONS> get_index_range;
  struct get_read_indices<DIMENSIONS, SUBDIV_DIM> get_read_indices;
  struct get_write_indices<DIMENSIONS, SUBDIV_DIM> get_write_indices;
  struct calculate_grid_size<DIMENSIONS> calculate_grid_size;
  struct sub_valarray<DIMENSIONS> sub_valarray;

  /// The error threshold value limiting the number of itrations.
  const double& epsilon;
};
}  // namespace psys2

#endif  // PSYS2_JACOBI_MPI_NON_BLOCKING_EXECUTOR_H