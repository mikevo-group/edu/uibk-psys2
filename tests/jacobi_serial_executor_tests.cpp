#include <gtest/gtest.h>

#include "psys2/algorithm.h"
#include "psys2/bounded_grid.h"
#include "psys2/grid.h"
#include "psys2/jacobi_serial_executor.h"

namespace psys2 {
TEST(JacobiSerialExecutor, execute_1D) {
  constexpr size_t DIM = 1;
  using grid_t = BoundedGrid<double, DIM>;
  using range_pair = std::pair<grid_t::index_t, grid_t::index_t>;

  grid_t::grid_size_type size = {{5}};
  grid_t::bound_t bounds = {{1.0, 2.0}};
  auto grids = init_bounded_grid<grid_t>(size, bounds);

  JacobiSerialExecutor<grid_t> executor(grids, 0.38);
  auto grid = executor.execute();

  std::array<double, 9> expected = {{0.875, 1.125, 1.375}};

  struct get_index_range<DIM> get_index_range;
  range_pair range = get_index_range(size);

  size_t expected_index = 0;
  for_each_index1D(range.first, range.second,
                   [&](const grid_t::index_t &index) {
                     EXPECT_FLOAT_EQ(expected[expected_index], grid->at(index));
                     ++expected_index;
                   });
}

TEST(JacobiSerialExecutor, execute_2D) {
  constexpr size_t DIM = 2;
  using grid_t = BoundedGrid<double, DIM>;
  using range_pair = std::pair<grid_t::index_t, grid_t::index_t>;

  grid_t::grid_size_type size = {{5, 5}};
  grid_t::bound_t bounds = {{1.0, 2.0, 3.0, 4.0}};
  auto grids = init_bounded_grid<grid_t>(size, bounds);

  JacobiSerialExecutor<grid_t> executor(grids, 5.0);
  auto grid = executor.execute();

  std::array<double, 9> expected = {
      {1.25, 0.8125, 1.5625, 1.3125, 0.625, 1.6875, 1.5625, 1.1875, 1.875}};

  struct get_index_range<DIM> get_index_range;
  range_pair range = get_index_range(size);

  size_t expected_index = 0;
  for_each_index2D(range.first, range.second,
                   [&](const grid_t::index_t &index) {
                     EXPECT_FLOAT_EQ(expected[expected_index], grid->at(index));
                     ++expected_index;
                   });
}

TEST(JacobiSerialExecutor, execute_3D) {
  constexpr size_t DIM = 3;
  using grid_t = BoundedGrid<double, DIM>;
  using range_pair = std::pair<grid_t::index_t, grid_t::index_t>;

  grid_t::grid_size_type size = {{4, 4, 4}};
  grid_t::bound_t bounds = {{1., 2., 3., 4., 5., 6.}};
  auto grids = init_bounded_grid<grid_t>(size, bounds);

  JacobiSerialExecutor<grid_t> executor(grids, 14.1);
  auto grid = executor.execute();

  std::array<double, 9> expected = {
      {1.5, 5. / 3., 5. / 3., 11. / 6., 5. / 3., 11. / 6., 11. / 6., 2.}};

  struct get_index_range<DIM> get_index_range;
  range_pair range = get_index_range(size);

  size_t expected_index = 0;
  for_each_index3D(range.first, range.second,
                   [&](const grid_t::index_t &index) {
                     EXPECT_FLOAT_EQ(expected[expected_index], grid->at(index));
                     ++expected_index;
                   });
}

}  // namespace psys2