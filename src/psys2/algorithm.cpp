#include "psys2/algorithm.h"

void psys2::for_each_index1D(const index1D_t& start, const index1D_t& end,
                             std::function<void(const index1D_t&)> const& f) {
  psys2::index1D_t index;
  for (index[0] = start[0]; index[0] <= end[0]; ++index[0]) {
    f(index);
  }
}

void psys2::for_each_index2D(const index2D_t& start, const index2D_t& end,
                             std::function<void(const index2D_t&)> const& f) {
  psys2::index2D_t index;
  for (index[0] = start[0]; index[0] <= end[0]; ++index[0]) {
    for (index[1] = start[1]; index[1] <= end[1]; ++index[1]) {
      f(index);
    }
  }
}

void psys2::for_each_index3D(const index3D_t& start, const index3D_t& end,
                             std::function<void(const index3D_t&)> const& f) {
  psys2::index3D_t index;
  for (index[0] = start[0]; index[0] <= end[0]; ++index[0]) {
    for (index[1] = start[1]; index[1] <= end[1]; ++index[1]) {
      for (index[2] = start[2]; index[2] <= end[2]; ++index[2]) {
        f(index);
      }
    }
  }
}