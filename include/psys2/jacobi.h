#ifndef PSYS2_JACOBI_H
#define PSYS2_JACOBI_H

#include "stencil.h"

namespace psys2 {

/**
 * Jacobi class used to calculate the "Jacobi" value of a cell in a grid.
 */
template <typename Cell, size_t dim>
class Jacobi {
 public:
  /// The stencil type used for the value calculations
  using stencil_t = Stencil<Cell, dim, 2 * dim>;

  /// The grid type of the underlaying grid.
  using grid_t = typename stencil_t::grid_t;

  /// The grid pointer type of the underlaying grid.
  using grid_ptr_t = typename stencil_t::grid_ptr_t;

  /// The index type for accessing grid cells.
  using index_t = typename stencil_t::index_t;

  /**
   * Creates a new jacobi class using the given grid as its underlaying grid.
   * @param  grid The underlaying grid used for its methods.
   * @return      The new jacobi object.
   */
  explicit Jacobi(grid_ptr_t grid) : stencil(grid, jacobi_stencil_shape()) {}

  /**
   * Replaces the underlaying grid with the given grid.
   * @param grid The new underlaying grid.
   */
  void replace_grid(grid_ptr_t grid) { stencil.replace_grid(grid); }

  /**
   * Calculates the "Jacobi" value of the cell at the given position.
   * @param  center The position of the cell.
   * @return        The "Jacobi" value of the cell at the given position.
   */
  auto calculate_value(const index_t& center) {
    auto stencil_values = stencil.stencil_values(center);

    Cell value = Cell();
    for (auto stencil_value : stencil_values) {
      value += stencil_value;
    }
    value /= stencil_values.size();

    return value;
  }

  /**
   * Generates the shape of the stencil for the Jacobi method.
   * @return The shape of the stencil for the Jacobi method.
   */
  static auto jacobi_stencil_shape() {
    typename stencil_t::member_t shape;

    for (size_t i = 0, index = 0; i < dim; ++i) {
      // Intizialize all elements with 0
      shape[index] = {{0}};
      shape[index][i] = 1;
      ++index;

      shape[index] = {{0}};
      shape[index][i] = -1;
      ++index;
    }

    return shape;
  }

 private:
  /// The stencil used to get the values for the "Jacobi" value calculation.
  stencil_t stencil;
};
}  // namespace psys2

#endif