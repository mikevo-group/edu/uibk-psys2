#include <chrono>
#include <iostream>
#include <string>
#include <utility>
#include <vector>

namespace bench {
/// A point in time.
using time_point = std::chrono::high_resolution_clock::time_point;

/// Time duration in ms
using duration_milli = std::chrono::duration<double, std::milli>;

/// Timer class.
class Timer {
 public:
  Timer() { reset(); }

  /// Starts the timer.
  virtual void start() {
    is_running = true;
    latest_start_time = std::chrono::high_resolution_clock::now();
  }

  /// Stops the timer.
  virtual void stop() {
    if (is_running) {
      auto end = std::chrono::high_resolution_clock::now();
      duration_milli latest_duration = end - latest_start_time;
      duration += latest_duration;
      is_running = false;
    }
  }

  /**
   * The total duration for which the timer timer was running.
   * @return Total duration.
   */
  duration_milli result() { return duration; }

  /// Resets the timer.
  virtual void reset() {
    is_running = false;
    duration = duration_milli::zero();
  }

 private:
  bool is_running;
  time_point latest_start_time;
  duration_milli duration;
};

/// Problem timer class including information of the benchmarked problem.
class ProblemTimer : public Timer {
 public:
  /// Seperator used for printing
  static constexpr auto DEFAULT_SEPERATOR = ",";

  /**
   * Creates a new problem timer.
   * @param name    Name of the problem.
   * @param dim     Dimensions of the problem.
   * @param size    Size of the problem.
   * @param epsilon Allowed error.
   * @param runs    Number of runs of the experiment.
   */
  ProblemTimer(const std::string name, const size_t dim, const size_t size,
               const double epsilon, const size_t runs)
      : name(name), dim(dim), size(size), epsilon(epsilon), runs(runs) {}

  /**
   * Prints the benchmark result of the benchmarked problem. The default format
   * is: <name>,<dim>,<size>,<allowed-error>,<avg-time>,<num-of-runs>
   * @param seperator Used column seperator. By default comma.
   */
  void print_result(std::string seperator = DEFAULT_SEPERATOR) {
    auto time_per_run = result() / runs;

    std::cout << name << seperator;
    std::cout << dim << seperator;
    std::cout << size << seperator;
    std::cout << epsilon << seperator;
    std::cout << time_per_run.count() << seperator;
    std::cout << runs << std::endl;
  }

  const std::string name;
  const size_t dim;
  const size_t size;
  const double epsilon;
  const size_t runs;
};

}  // namespace bench