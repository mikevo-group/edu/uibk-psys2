## Contributing
This is a university course project, therefore please do not contribute to this project if you are not a member of this project. Nevertheless, feel free to fork and use the code in a way permitted by the granted license.

Virtual Conference Room: [appear.in/uibk-psys2](https://appear.in/uibk-psys2)