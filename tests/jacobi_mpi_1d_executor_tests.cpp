#include <gtest/gtest.h>

#include "psys2/jacobi_mpi_executor.h"
#include "psys2/jacobi_serial_executor.h"

namespace psys2 {

const MPI_Comm communicator = MPI_COMM_WORLD;

TEST(JacobiMPI1DExecutor, execute_1D) {
  MPI_Barrier(communicator);
  constexpr size_t DIM = 1;

  using grid_t = BoundedGrid<double, DIM>;
  using grid_ptr_t = JacobiMPIExecutor<DIM, 1>::grid_ptr_t;
  using range_pair = std::pair<grid_t::index_t, grid_t::index_t>;

  grid_t::grid_size_type size = {{5}};
  grid_t::bound_t bounds = {{1.0, 2.0}};
  grid_ptr_t grid = std::make_shared<grid_t>(size, bounds);

  size_t world_size = mpi_comm_size(communicator);
  if (world_size > 1) {
    --world_size;
  }

  JacobiMPIExecutor<DIM, 1> executor(grid, 0.38, world_size);
  grid = executor.execute();

  if (mpi_rank(communicator) == ROOT_RANK) {
    std::array<double, 9> expected = {{0.875, 1.125, 1.375}};

    struct get_index_range<DIM> get_index_range;
    range_pair range = get_index_range(size);

    size_t expected_index = 0;
    for_each_index1D(
        range.first, range.second, [&](const grid_t::index_t &index) {
          EXPECT_FLOAT_EQ(expected[expected_index], grid->at(index));
          ++expected_index;
        });
  }
  MPI_Barrier(communicator);
}

TEST(JacobiMPI1DExecutor, execute_1D__comp_to_serial) {
  MPI_Barrier(communicator);
  constexpr size_t DIM = 1;
  constexpr double ERROR = 0.23;
  using grid_t = BoundedGrid<double, DIM>;
  using grid_ptr_t = JacobiMPIExecutor<DIM, 1>::grid_ptr_t;
  using range_pair = std::pair<grid_t::index_t, grid_t::index_t>;

  grid_t::grid_size_type size = {
      {2713}};  // prime to ensure bad split condition
  grid_t::bound_t bounds = {{1.0, 2.0}};

  grid_ptr_t grid = std::make_shared<grid_t>(size, bounds);
  JacobiMPIExecutor<DIM, 1> mpi_executor(grid, ERROR,
                                         mpi_comm_size(communicator));
  grid = mpi_executor.execute();

  if (mpi_rank(communicator) == ROOT_RANK) {
    auto grids = init_bounded_grid<grid_t>(size, bounds);
    JacobiSerialExecutor<grid_t> serial_executor(grids, ERROR);
    auto expected = serial_executor.execute();

    struct get_index_range<DIM> get_index_range;
    range_pair range = get_index_range(size);

    for_each_index1D(range.first, range.second,
                     [&](const grid_t::index_t &index) {
                       EXPECT_FLOAT_EQ(expected->at(index), grid->at(index));
                     });
  }
  MPI_Barrier(communicator);
}

TEST(JacobiMPI1DExecutor, execute_2D__comp_to_serial) {
  MPI_Barrier(communicator);
  constexpr size_t DIM = 2;
  constexpr double ERROR = 123.;
  using grid_t = BoundedGrid<double, DIM>;
  using grid_ptr_t = JacobiMPIExecutor<DIM, 1>::grid_ptr_t;
  using range_pair = std::pair<grid_t::index_t, grid_t::index_t>;

  grid_t::grid_size_type size = {{109, 113}};  // primes for bad split condition
  grid_t::bound_t bounds = {{1.0, 2.0, 3.0, 4.0}};

  grid_ptr_t grid = std::make_shared<grid_t>(size, bounds);
  JacobiMPIExecutor<DIM, 1> mpi_executor(grid, ERROR,
                                         mpi_comm_size(communicator));
  grid = mpi_executor.execute();

  if (mpi_rank(communicator) == ROOT_RANK) {
    auto grids = init_bounded_grid<grid_t>(size, bounds);
    JacobiSerialExecutor<grid_t> serial_executor(grids, ERROR);
    auto expected = serial_executor.execute();

    struct get_index_range<DIM> get_index_range;
    range_pair range = get_index_range(size);

    for_each_index2D(range.first, range.second,
                     [&](const grid_t::index_t &index) {
                       EXPECT_FLOAT_EQ(expected->at(index), grid->at(index));
                     });
  }
  MPI_Barrier(communicator);
}

TEST(JacobiMPI1DExecutor, execute_3D__comp_to_serial) {
  MPI_Barrier(communicator);
  constexpr size_t DIM = 3;
  constexpr double ERROR = 12345.;
  using grid_t = BoundedGrid<double, DIM>;
  using grid_ptr_t = JacobiMPIExecutor<DIM, 1>::grid_ptr_t;
  using range_pair = std::pair<grid_t::index_t, grid_t::index_t>;

  grid_t::grid_size_type size = {{31, 37, 41}};  // primes for bad split
  grid_t::bound_t bounds = {{1.0, 2.0, 3.0, 4.0, 5.0, 6.0}};

  grid_ptr_t grid = std::make_shared<grid_t>(size, bounds);
  JacobiMPIExecutor<DIM, 1> mpi_executor(grid, ERROR,
                                         mpi_comm_size(communicator));
  grid = mpi_executor.execute();

  if (mpi_rank(communicator) == ROOT_RANK) {
    auto grids = init_bounded_grid<grid_t>(size, bounds);
    JacobiSerialExecutor<grid_t> serial_executor(grids, ERROR);
    auto expected = serial_executor.execute();

    struct get_index_range<DIM> get_index_range;
    range_pair range = get_index_range(size);

    for_each_index3D(range.first, range.second,
                     [&](const grid_t::index_t &index) {
                       EXPECT_FLOAT_EQ(expected->at(index), grid->at(index));
                     });
  }
  MPI_Barrier(communicator);
}

}  // namespace psys2