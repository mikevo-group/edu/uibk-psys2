#ifndef PSYS2_BOUNDED_GRID_H
#define PSYS2_BOUNDED_GRID_H

#include <array>

#include "grid.h"

namespace psys2 {
/**
 * Type used to store the bounds of the grid.
 */
template <typename Cell, size_t dim>
using bounded_grid_bound_t = std::array<Cell, 2 * dim>;

/**
 * Creates a pair of bounded grids with the specified size.
 * @param  size   Size of each grid.
 * @param  bounds Bounds for each grid.
 * @return        The two bounded grids.
 */
template <typename grid_t>
auto init_bounded_grid(const typename grid_t::grid_size_type& size,
                       const typename grid_t::bound_t& bounds) {
  auto grid0 = std::make_shared<grid_t>(size, bounds);
  auto grid1 = std::make_shared<grid_t>(size, bounds);

  return std::make_pair(grid0, grid1);
}

template <typename Cell, size_t dim>
class BoundedGrid : public Grid<Cell, dim> {
 public:
  using parent_t = Grid<Cell, dim>;
  using bound_t = bounded_grid_bound_t<Cell, dim>;
  using grid_size_type = grid_size_t<dim>;
  using index_t = grid_index_t<dim>;

  /// Number of dimensions the grid has.
  static constexpr auto DIMENSIONS = dim;

  using parent_t::linear_index_of;

  explicit BoundedGrid(const grid_size_type& size, const bound_t& bounds)
      : parent_t::Grid(remove_size_offset(size)),
        bounds(bounds),
        dimension_size(size) {}

  /**
   * Data access method of the grid with the tiebreaker that the lower dimension
   * wins, e.g. at corner points.
   * @param  index The N-dimensional index specifying the position in the
   *               grid.
   * @return       The cell item at this position. The tiebreaker for bound
   *               values, e.g. for corner points, is that the lower dimension
   *               wins.
   */
  virtual Cell& at(const index_t& index) {
    if (index_in_grid(index))
      return parent_t::at(remove_index_offset(index));
    else
      return bound_value(index);
  }

  /**
   * Stores the given item at the specified index and ignores stores to a bound
   * cell.
   * @param index The N-dimensional index specifying the position in the
   *              grid.
   * @param item  The cell item to be stored.
   * @return      True if the write was successful, otherwise false (e.g. if the
   *              index points to a bound cell or outside the grid).
   */
  virtual bool store(const grid_index_t<dim>& index, Cell item) {
    if (index_in_grid(index)) {
      parent_t::store(remove_index_offset(index), item);
      return true;
    }

    return false;
  }

  /**
   * Accesses the size of the grid.
   * @return The size of the grid including bounds.
   */
  virtual const grid_size_type size() const { return dimension_size; }

 private:
  bound_t bounds;
  const grid_size_type dimension_size;

  bool index_in_grid(const index_t& index) {
    for (size_t i = 0; i < parent_t::DIMENSIONS; ++i) {
      size_t dim_size = dimension_size[i];
      if ((index[i] < 1) || (index[i] >= dim_size - 1)) return false;
    }

    return true;
  }

  auto& bound_value(const index_t& index) {
    size_t bound_index = 0;
    for (size_t i = 0; i < parent_t::DIMENSIONS; ++i) {
      if (index[i] < 1) return bounds[bound_index];
      ++bound_index;

      size_t dim_size = dimension_size[i];
      if (index[i] >= dim_size - 1) return bounds[bound_index];
      ++bound_index;
    }

    // fallback that should never be used
    return bounds[0];
  }

  static auto remove_index_offset(const index_t& index) {
    index_t storage_index = index;

    for (size_t i = 0; i < parent_t::DIMENSIONS; ++i) {
      --storage_index[i];
    }

    return storage_index;
  }

  static auto remove_size_offset(const grid_size_type& size) {
    grid_size_type storage_size;

    for (size_t i = 0; i < parent_t::DIMENSIONS; ++i) {
      storage_size[i] = size[i] - 2;
    }

    return storage_size;
  }
};
}  // namespace psys2

#endif