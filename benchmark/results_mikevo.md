### Serial

| Algorithm     | Grid Dimensions | Dimension Size | Epsilon | Run time in ms | Number of runs | 
|---------------|-----------------|----------------|---------|----------------|----------------| 
| jacobi_serial | 1               | 65536          | 2       | 1.17629        | 10             | 
| jacobi_serial | 1               | 131072         | 1       | 1.99875        | 10             | 
|               |                 |                |         |                |                | 
| jacobi_serial | 2               | 512            | 20      | 1444.68        | 10             | 
| jacobi_serial | 2               | 512            | 10      | 5220.24        | 10             | 
| jacobi_serial | 2               | 768            | 20      | 7270.98        | 10             | 
| jacobi_serial | 2               | 768            | 100     | 323.24         | 10             | 
|               |                 |                |         |                |                | 
| jacobi_serial | 3               | 128            | 7000    | 409.964        | 10             | 
| jacobi_serial | 3               | 128            | 5000    | 827.12         | 10             | 
| jacobi_serial | 3               | 128            | 2000    | 4343.71        | 10             | 

### 1D Subdivision

| Algorithm                   | Grid Dimensions | Dimension Size | Epsilon | Run time in ms | Number of runs | Speedup compared to Serial | 
|-----------------------------|-----------------|----------------|---------|----------------|----------------|----------------------------| 
| jacobi_mpi_1d_subdivision_1 | 2               | 512            | 20      | 4681.44        | 10             | 0.309                      | 
| jacobi_mpi_1d_subdivision_1 | 2               | 512            | 10      | 17184          | 10             | 0.304                      | 
| jacobi_mpi_1d_subdivision_1 | 2               | 768            | 20      | 23888.7        | 10             | 0.304                      | 
| jacobi_mpi_1d_subdivision_1 | 2               | 768            | 100     | 1066.07        | 10             | 0.303                      | 
|                             |                 |                |         |                |                |                            | 
| jacobi_mpi_1d_subdivision_2 | 2               | 512            | 20      | 3631.31        | 10             | 0.398                      | 
| jacobi_mpi_1d_subdivision_2 | 2               | 512            | 10      | 13271.1        | 10             | 0.393                      | 
| jacobi_mpi_1d_subdivision_2 | 2               | 768            | 20      | 18555          | 10             | 0.392                      | 
| jacobi_mpi_1d_subdivision_2 | 2               | 768            | 100     | 830.763        | 10             | 0.389                      | 
|                             |                 |                |         |                |                |                            | 
| jacobi_mpi_1d_subdivision_4 | 2               | 512            | 20      | 3227.89        | 10             | 0.448                      | 
| jacobi_mpi_1d_subdivision_4 | 2               | 512            | 10      | 11779.3        | 10             | 0.443                      | 
| jacobi_mpi_1d_subdivision_4 | 2               | 768            | 20      | 16368.9        | 10             | 0.444                      | 
| jacobi_mpi_1d_subdivision_4 | 2               | 768            | 100     | 734.682        | 10             | 0.440                      | 
|                             |                 |                |         |                |                |                            | 
| jacobi_mpi_1d_subdivision_8 | 2               | 512            | 20      | 4723.25        | 10             | 0.306                      | 
| jacobi_mpi_1d_subdivision_8 | 2               | 512            | 10      | 17872.4        | 10             | 0.292                      | 
| jacobi_mpi_1d_subdivision_8 | 2               | 768            | 20      | 23955.6        | 10             | 0.304                      | 
| jacobi_mpi_1d_subdivision_8 | 2               | 768            | 100     | 1087.68        | 10             | 0.297                      | 

![1D Subdivision](1d_subdivision_mikevo.png)

### 2D Subdivision

| Algorithm                   | Grid Dimensions | Dimension Size | Epsilon | Run time in ms | Number of runs | Speedup compared to Serial | Speedup compared to 1D Subdivision | 
|-----------------------------|-----------------|----------------|---------|----------------|----------------|----------------------------|------------------------------------| 
| jacobi_mpi_2d_subdivision_1 | 2               | 512            | 20      | 4589.67        | 10             | 0.315                      | 1.020                              | 
| jacobi_mpi_2d_subdivision_1 | 2               | 512            | 10      | 16853.6        | 10             | 0.310                      | 1.020                              | 
| jacobi_mpi_2d_subdivision_1 | 2               | 768            | 20      | 24089.9        | 10             | 0.302                      | 0.992                              | 
| jacobi_mpi_2d_subdivision_1 | 2               | 768            | 100     | 1073.83        | 10             | 0.301                      | 0.993                              | 
|                             |                 |                |         |                |                |                            |                                    | 
| jacobi_mpi_2d_subdivision_2 | 2               | 512            | 20      | 3650.44        | 10             | 0.396                      | 0.995                              | 
| jacobi_mpi_2d_subdivision_2 | 2               | 512            | 10      | 13357.1        | 10             | 0.391                      | 0.994                              | 
| jacobi_mpi_2d_subdivision_2 | 2               | 768            | 20      | 18664.3        | 10             | 0.390                      | 0.994                              | 
| jacobi_mpi_2d_subdivision_2 | 2               | 768            | 100     | 833.831        | 10             | 0.388                      | 0.996                              | 
|                             |                 |                |         |                |                |                            |                                    | 
| jacobi_mpi_2d_subdivision_4 | 2               | 512            | 20      | 3241.89        | 10             | 0.446                      | 0.996                              | 
| jacobi_mpi_2d_subdivision_4 | 2               | 512            | 10      | 11869          | 10             | 0.440                      | 0.992                              | 
| jacobi_mpi_2d_subdivision_4 | 2               | 768            | 20      | 16565.5        | 10             | 0.439                      | 0.988                              | 
| jacobi_mpi_2d_subdivision_4 | 2               | 768            | 100     | 742.951        | 10             | 0.435                      | 0.989                              | 
|                             |                 |                |         |                |                |                            |                                    | 
| jacobi_mpi_2d_subdivision_8 | 2               | 512            | 20      | 4739.56        | 10             | 0.305                      | 0.997                              | 
| jacobi_mpi_2d_subdivision_8 | 2               | 512            | 10      | 17155.7        | 10             | 0.304                      | 1.042                              | 
| jacobi_mpi_2d_subdivision_8 | 2               | 768            | 20      | 23803.2        | 10             | 0.305                      | 1.006                              | 
| jacobi_mpi_2d_subdivision_8 | 2               | 768            | 100     | 1051.6         | 10             | 0.307                      | 1.034                              | 

![2D Subdivision](2d_subdivision_mikevo.png)

### 2D Subdivision Non-Blocking

| Algorithm                                | Grid Dimensions | Dimension Size | Epsilon | Run time in ms | Number of runs | Speedup compared to Serial | Speedup compared to 1D Subdivision | Speedup compared to 2D Subdivision | 
|------------------------------------------|-----------------|----------------|---------|----------------|----------------|----------------------------|------------------------------------|------------------------------------| 
| jacobi_mpi_2d_subdivision_non_blocking_1 | 2               | 512            | 20      | 4408.49        | 10             | 0.328                      | 1.062                              | 1.041                              | 
| jacobi_mpi_2d_subdivision_non_blocking_1 | 2               | 512            | 10      | 16119.9        | 10             | 0.324                      | 1.066                              | 1.046                              | 
| jacobi_mpi_2d_subdivision_non_blocking_1 | 2               | 768            | 20      | 23045.9        | 10             | 0.315                      | 1.037                              | 1.045                              | 
| jacobi_mpi_2d_subdivision_non_blocking_1 | 2               | 768            | 100     | 1027.55        | 10             | 0.315                      | 1.037                              | 1.045                              | 
|                                          |                 |                |         |                |                |                            |                                    |                                    | 
| jacobi_mpi_2d_subdivision_non_blocking_2 | 2               | 512            | 20      | 3581.35        | 10             | 0.403                      | 1.014                              | 1.019                              | 
| jacobi_mpi_2d_subdivision_non_blocking_2 | 2               | 512            | 10      | 13090.2        | 10             | 0.399                      | 1.014                              | 1.020                              | 
| jacobi_mpi_2d_subdivision_non_blocking_2 | 2               | 768            | 20      | 18235.6        | 10             | 0.399                      | 1.018                              | 1.024                              | 
| jacobi_mpi_2d_subdivision_non_blocking_2 | 2               | 768            | 100     | 827.735        | 10             | 0.391                      | 1.004                              | 1.007                              | 
|                                          |                 |                |         |                |                |                            |                                    |                                    | 
| jacobi_mpi_2d_subdivision_non_blocking_4 | 2               | 512            | 20      | 3208.73        | 10             | 0.450                      | 1.006                              | 1.010                              | 
| jacobi_mpi_2d_subdivision_non_blocking_4 | 2               | 512            | 10      | 11775.1        | 10             | 0.443                      | 1.000                              | 1.008                              | 
| jacobi_mpi_2d_subdivision_non_blocking_4 | 2               | 768            | 20      | 16364.6        | 10             | 0.444                      | 1.000                              | 1.012                              | 
| jacobi_mpi_2d_subdivision_non_blocking_4 | 2               | 768            | 100     | 745.421        | 10             | 0.434                      | 0.986                              | 0.997                              | 
|                                          |                 |                |         |                |                |                            |                                    |                                    | 
| jacobi_mpi_2d_subdivision_non_blocking_8 | 2               | 512            | 20      | 4615.58        | 10             | 0.313                      | 1.023                              | 1.027                              | 
| jacobi_mpi_2d_subdivision_non_blocking_8 | 2               | 512            | 10      | 16858.9        | 10             | 0.310                      | 1.060                              | 1.018                              | 
| jacobi_mpi_2d_subdivision_non_blocking_8 | 2               | 768            | 20      | 23577.6        | 10             | 0.308                      | 1.016                              | 1.010                              | 
| jacobi_mpi_2d_subdivision_non_blocking_8 | 2               | 768            | 100     | 1096.43        | 10             | 0.295                      | 0.992                              | 0.959                              | 

![2D Subdivision Non-Blocking](2d_subdivision_non_blocking_mikevo.png)

### 2D Subdivision Ghost Cells

| Algorithm                              | Grid Dimensions | Dimension Size | Epsilon | Run time in ms | Number of runs | Speedup compared to Serial | Speedup compared to 1D Subdivision | Speedup compared to 2D Subdivision | Speedup compared to 2D Subdivision Non-Blocking | 
|----------------------------------------|-----------------|----------------|---------|----------------|----------------|----------------------------|------------------------------------|------------------------------------|-------------------------------------------------| 
| jacobi_mpi_2d_subdivision_ghost_cell_1 | 2               | 512            | 20      | 6233.01        | 10             | 0.232                      | 0.751                              | 0.736                              | 0.707                                           | 
| jacobi_mpi_2d_subdivision_ghost_cell_1 | 2               | 512            | 10      | 22943.4        | 10             | 0.228                      | 0.749                              | 0.735                              | 0.703                                           | 
| jacobi_mpi_2d_subdivision_ghost_cell_1 | 2               | 768            | 20      | 31970.5        | 10             | 0.227                      | 0.747                              | 0.754                              | 0.721                                           | 
| jacobi_mpi_2d_subdivision_ghost_cell_1 | 2               | 768            | 100     | 1405.98        | 10             | 0.230                      | 0.758                              | 0.764                              | 0.731                                           | 
|                                        |                 |                |         |                |                |                            |                                    |                                    |                                                 | 
| jacobi_mpi_2d_subdivision_ghost_cell_2 | 2               | 512            | 20      | 3333.37        | 10             | 0.433                      | 1.089                              | 1.095                              | 1.074                                           | 
| jacobi_mpi_2d_subdivision_ghost_cell_2 | 2               | 512            | 10      | 12230          | 10             | 0.427                      | 1.085                              | 1.092                              | 1.070                                           | 
| jacobi_mpi_2d_subdivision_ghost_cell_2 | 2               | 768            | 20      | 17057.3        | 10             | 0.426                      | 1.088                              | 1.094                              | 1.069                                           | 
| jacobi_mpi_2d_subdivision_ghost_cell_2 | 2               | 768            | 100     | 773.389        | 10             | 0.418                      | 1.074                              | 1.078                              | 1.070                                           | 
|                                        |                 |                |         |                |                |                            |                                    |                                    |                                                 | 
| jacobi_mpi_2d_subdivision_ghost_cell_4 | 2               | 512            | 20      | 1720.28        | 10             | 0.840                      | 1.876                              | 1.885                              | 1.865                                           | 
| jacobi_mpi_2d_subdivision_ghost_cell_4 | 2               | 512            | 10      | 6281.77        | 10             | 0.831                      | 1.875                              | 1.889                              | 1.874                                           | 
| jacobi_mpi_2d_subdivision_ghost_cell_4 | 2               | 768            | 20      | 8690.37        | 10             | 0.837                      | 1.884                              | 1.906                              | 1.883                                           | 
| jacobi_mpi_2d_subdivision_ghost_cell_4 | 2               | 768            | 100     | 410.59         | 10             | 0.787                      | 1.789                              | 1.809                              | 1.815                                           | 
|                                        |                 |                |         |                |                |                            |                                    |                                    |                                                 | 
| jacobi_mpi_2d_subdivision_ghost_cell_8 | 2               | 512            | 20      | 1653.91        | 10             | 0.873                      | 2.856                              | 2.866                              | 2.791                                           | 
| jacobi_mpi_2d_subdivision_ghost_cell_8 | 2               | 512            | 10      | 6224.59        | 10             | 0.839                      | 2.871                              | 2.756                              | 2.708                                           | 
| jacobi_mpi_2d_subdivision_ghost_cell_8 | 2               | 768            | 20      | 8663.55        | 10             | 0.839                      | 2.765                              | 2.748                              | 2.721                                           | 
| jacobi_mpi_2d_subdivision_ghost_cell_8 | 2               | 768            | 100     | 417.04         | 10             | 0.775                      | 2.608                              | 2.522                              | 2.629                                           | 

![2D Subdivision Ghost Cells](2d_subdivision_ghost_cell_mikevo.png)
