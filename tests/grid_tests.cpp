#include <gtest/gtest.h>

#include <vector>

#include "psys2/grid.h"

namespace psys2 {

TEST(Grid, dimensions) {
  auto dims = Grid<int, 1>::DIMENSIONS;
  EXPECT_EQ(1, dims);

  dims = Grid<int, 2>::DIMENSIONS;
  EXPECT_EQ(2, dims);

  dims = Grid<int, 3>::DIMENSIONS;
  EXPECT_EQ(3, dims);
}

TEST(Grid, dimension_size) {
  grid_size_t<3> expected = {{2, 3, 4}};
  Grid<int, 3> g(expected);
  auto size = g.size();
  EXPECT_EQ(expected, size);
}

TEST(Grid, grid_size_type) {
  const grid_size_t<1> t1 = {{1}};
  EXPECT_EQ(1, t1.size());
  EXPECT_EQ(1, t1.at(0));

  const grid_size_t<2> t2 = {{1, 2}};
  EXPECT_EQ(2, t2.size());
  EXPECT_EQ(1, t2.at(0));
  EXPECT_EQ(2, t2.at(1));

  const grid_size_t<3> t3 = {{1, 2, 3}};
  EXPECT_EQ(3, t3.size());
  EXPECT_EQ(1, t3.at(0));
  EXPECT_EQ(2, t3.at(1));
  EXPECT_EQ(3, t3.at(2));
}

TEST(Grid, grid_index_t) {
  const grid_index_t<3> index = {{1, 2, 3}};
  EXPECT_EQ(3, index.size());
  EXPECT_EQ(1, index.at(0));
  EXPECT_EQ(2, index.at(1));
  EXPECT_EQ(3, index.at(2));
}

TEST(Grid, linear_size) {
  const grid_size_t<3> size = {{2, 3, 4}};
  EXPECT_EQ(24, linear_size(size));
}

TEST(Grid, is_bound_index__1D) {
  constexpr size_t DIM = 1;
  const grid_index_t<DIM> start = {{3}};
  const grid_index_t<DIM> end = {{15}};

  std::array<bool, 13> expected = {{true, false, false, false, false, false,
                                    false, false, false, false, false, false,
                                    true}};
  size_t i = 0;
  for_each_index1D(
      start, end,
      [&start, &end, &expected, &i](const grid_index_t<DIM>& index) {
        EXPECT_EQ(expected[i], is_bound_index(start, end, index));
        ++i;
      });
}

TEST(Grid, is_bound_index__2D) {
  constexpr size_t DIM = 2;
  const grid_index_t<DIM> start = {{3, 3}};
  const grid_index_t<DIM> end = {{7, 7}};

  std::array<bool, 25> expected = {
      {true,  true, true,  true,  true,  true, false, false, false,
       true,  true, false, false, false, true, true,  false, false,
       false, true, true,  true,  true,  true, true}};
  size_t i = 0;
  for_each_index2D(
      start, end,
      [&start, &end, &expected, &i](const grid_index_t<DIM>& index) {
        EXPECT_EQ(expected[i], is_bound_index(start, end, index));
        ++i;
      });
}

TEST(Grid, is_bound_index__3D) {
  constexpr size_t DIM = 3;
  const grid_index_t<DIM> start = {{3, 3, 3}};
  const grid_index_t<DIM> end = {{5, 5, 5}};

  std::array<bool, 27> expected = {{true, true, true, true, true, true, true,
                                    true, true, true, true, true, true, false,
                                    true, true, true, true, true, true, true,
                                    true, true, true, true, true, true}};
  size_t i = 0;
  for_each_index3D(
      start, end,
      [&start, &end, &expected, &i](const grid_index_t<DIM>& index) {
        EXPECT_EQ(expected[i], is_bound_index(start, end, index));
        ++i;
      });
}

TEST(Grid, for_each_bound_index__1D) {
  constexpr size_t DIM = 1;
  const grid_index_t<DIM> start = {{3}};
  const grid_index_t<DIM> end = {{1013}};

  std::vector<grid_index_t<DIM>> expected;
  for_each_index1D(start, end, [&](const grid_index_t<DIM>& index) {
    if (is_bound_index(start, end, index)) {
      expected.push_back(index);
    }
  });

  std::vector<grid_index_t<DIM>> actual;
  struct for_each_bound_index<DIM> for_each_bound;
  for_each_bound(start, end, [&actual](const grid_index_t<DIM>& index) {
    actual.push_back(index);
  });

  EXPECT_EQ(expected, actual);
}

TEST(Grid, for_each_bound_index__2D) {
  constexpr size_t DIM = 2;
  const grid_index_t<DIM> start = {{3, 5}};
  const grid_index_t<DIM> end = {{223, 233}};

  std::vector<grid_index_t<DIM>> expected;
  for_each_index2D(start, end, [&](const grid_index_t<DIM>& index) {
    if (is_bound_index(start, end, index)) {
      expected.push_back(index);
    }
  });

  std::vector<grid_index_t<DIM>> actual;
  struct for_each_bound_index<DIM> for_each_bound;
  for_each_bound(start, end, [&actual](const grid_index_t<DIM>& index) {
    actual.push_back(index);
  });

  EXPECT_EQ(expected, actual);
}

TEST(Grid, for_each_bound_index__3D) {
  constexpr size_t DIM = 3;
  const grid_index_t<DIM> start = {{3, 5, 7}};
  const grid_index_t<DIM> end = {{223, 233, 227}};

  std::vector<grid_index_t<DIM>> expected;
  for_each_index3D(start, end, [&](const grid_index_t<DIM>& index) {
    if (is_bound_index(start, end, index)) {
      expected.push_back(index);
    }
  });

  std::vector<grid_index_t<DIM>> actual;
  struct for_each_bound_index<DIM> for_each_bound;
  for_each_bound(start, end, [&actual](const grid_index_t<DIM>& index) {
    actual.push_back(index);
  });

  EXPECT_EQ(expected, actual);
}

TEST(Grid, linear_index_of) {
  const grid_size_t<3> size = {{4, 5, 6}};
  Grid<int, 3> g(size);

  grid_index_t<3> index;
  for (index[0] = 0; index[0] < size.at(0); ++index[0]) {
    for (index[1] = 0; index[1] < size.at(1); ++index[1]) {
      for (index[2] = 0; index[2] < size.at(2); ++index[2]) {
        auto expected = index[0] * size.at(1) * size.at(2) +
                        index[1] * size.at(2) + index[2];
        EXPECT_EQ(expected, g.linear_index_of(index));
      }
    }
  }
}

TEST(Grid, at) {
  const grid_size_t<3> size = {{4, 5, 6}};
  Grid<int, 3> g(size);
  grid_index_t<3> index;
  for (index[0] = 0; index[0] < size.at(0); ++index[0]) {
    for (index[1] = 0; index[1] < size.at(1); ++index[1]) {
      for (index[2] = 0; index[2] < size.at(2); ++index[2]) {
        EXPECT_EQ(0, g.at(index));
      }
    }
  }
}

TEST(Grid, store) {
  const grid_size_t<3> size = {{4, 5, 6}};
  Grid<int, 3> g(size);
  grid_index_t<3> index;
  for (index[0] = 0; index[0] < size.at(0); ++index[0]) {
    for (index[1] = 0; index[1] < size.at(1); ++index[1]) {
      for (index[2] = 0; index[2] < size.at(2); ++index[2]) {
        g.store(index, 1);
        EXPECT_EQ(1, g.at(index));
      }
    }
  }
}

TEST(Grid, subgrid) {
  constexpr size_t DIM = 2;
  const grid_size_t<DIM> size = {{5, 5}};
  Grid<int, DIM> grid(size);

  grid_index_t<DIM> index;
  size_t i = 1;
  for (index[0] = 0; index[0] < size.at(0); ++index[0]) {
    for (index[1] = 0; index[1] < size.at(1); ++index[1]) {
      grid.store(index, i);
      ++i;
    }
  }

  grid_index_t<DIM> start = {{2, 2}};
  grid_index_t<DIM> end = {{4, 4}};

  auto subgrid = grid.subgrid(start, end);

  std::vector<int> expected = {{13, 14, 15, 18, 19, 20, 23, 24, 25}};

  start = {{0, 0}};
  end = {{2, 2}};
  i = 0;

  struct for_each_index<DIM> for_each;
  for_each(start, end,
           [subgrid, &expected, &i](const grid_index_t<DIM>& index) {
             EXPECT_EQ(expected[i], subgrid->at(index));
             ++i;
           });
}

TEST(Grid, replace_with_subgrid) {
  constexpr size_t DIM = 2;
  const grid_size_t<DIM> size = {{5, 5}};
  Grid<int, DIM> grid(size);

  std::vector<int> expected;

  grid_index_t<DIM> index;
  int i = 1;
  for (index[0] = 0; index[0] < size.at(0); ++index[0]) {
    for (index[1] = 0; index[1] < size.at(1); ++index[1]) {
      grid.store(index, i);
      expected.push_back(i);
      ++i;
    }
  }

  std::vector<int> indices = {{13, 14, 15, 18, 19, 20, 23, 24, 25}};
  for (int index : indices) {
    expected[index - 1] = 0;
  }

  const grid_size_t<DIM> subgrid_size = {{3, 3}};
  auto subgrid = std::make_shared<Grid<int, DIM>>(subgrid_size);

  grid_index_t<DIM> start = {{2, 2}};
  grid.replace_with_subgrid(subgrid, start);

  start = {{0, 0}};
  grid_index_t<DIM> end = {{4, 4}};
  i = 0;

  struct for_each_index<DIM> for_each;
  for_each(start, end, [&grid, &expected, &i](const grid_index_t<DIM>& index) {
    EXPECT_EQ(expected[i], grid.at(index));
    ++i;
  });
}

TEST(Grid, boundary_data) {
  constexpr size_t DIM = 2;
  const grid_size_t<DIM> size = {{5, 5}};
  Grid<int, DIM> grid(size);

  grid_index_t<DIM> index;
  size_t i = 1;
  for (index[0] = 0; index[0] < size.at(0); ++index[0]) {
    for (index[1] = 0; index[1] < size.at(1); ++index[1]) {
      grid.store(index, i);
      ++i;
    }
  }

  grid_index_t<DIM> start = {{1, 1}};
  grid_index_t<DIM> end = {{4, 4}};
  std::vector<int> expected = {{7, 8, 9, 10, 12, 15, 17, 20, 22, 23, 24, 25}};

  auto actual = grid.boundary_data(start, end);

  EXPECT_EQ(expected, *actual);
}

TEST(Grid, replace_with_boundary_data) {
  constexpr size_t DIM = 2;
  const grid_size_t<DIM> size = {{5, 5}};
  Grid<int, DIM> grid(size);

  grid_index_t<DIM> index;
  size_t i = 1;
  for (index[0] = 0; index[0] < size.at(0); ++index[0]) {
    for (index[1] = 0; index[1] < size.at(1); ++index[1]) {
      grid.store(index, i);
      ++i;
    }
  }

  grid_index_t<DIM> start = {{1, 1}};
  grid_index_t<DIM> end = {{4, 4}};

  auto data = std::make_shared<std::vector<int>>();
  for (size_t i = 1; i <= 12; ++i) {
    data->push_back(i);
  }

  auto actual_return = grid.replace_with_boundary_data(data, start, end);
  EXPECT_TRUE(actual_return);

  std::vector<int> expected = {{1,  2, 3,  4, 5,  6,  1, 2,  3, 4,  11, 5, 13,
                                14, 6, 16, 7, 18, 19, 8, 21, 9, 10, 11, 12}};

  i = 0;

  start = {{0, 0}};
  end = {{4, 4}};
  struct for_each_index<DIM> for_each;
  for_each(start, end, [&grid, &expected, &i](const grid_index_t<DIM>& index) {
    EXPECT_EQ(expected[i], grid.at(index));
    ++i;
  });
}

TEST(Grid, calculate_bound_size__1D) {
  constexpr size_t DIM = 1;

  struct calculate_bound_size<DIM> calculate_bound_size;

  grid_index_t<DIM> start = {{3}};
  grid_index_t<DIM> end = {{9}};

  int expected = 2;
  int actual = calculate_bound_size(start, end);

  EXPECT_EQ(expected, actual);

  end = {{3}};
  expected = 1;
  actual = calculate_bound_size(start, end);

  EXPECT_EQ(expected, actual);
}

TEST(Grid, calculate_bound_size__2D) {
  constexpr size_t DIM = 2;

  struct calculate_bound_size<DIM> calculate_bound_size;

  grid_index_t<DIM> start = {{3, 9}};
  grid_index_t<DIM> end = {{13, 17}};

  int expected = 36;
  int actual = calculate_bound_size(start, end);

  EXPECT_EQ(expected, actual);

  end = {{3, 10}};
  expected = 2;
  actual = calculate_bound_size(start, end);

  EXPECT_EQ(expected, actual);
}

TEST(Grid, calculate_bound_size__3D) {
  constexpr size_t DIM = 3;

  struct calculate_bound_size<DIM> calculate_bound_size;

  grid_index_t<DIM> start = {{3, 9, 13}};
  grid_index_t<DIM> end = {{17, 21, 27}};

  int expected = 1066;
  int actual = calculate_bound_size(start, end);

  EXPECT_EQ(expected, actual);

  end = {{3, 10, 15}};
  expected = 6;
  actual = calculate_bound_size(start, end);

  EXPECT_EQ(expected, actual);
}

}  // namespace psys2
